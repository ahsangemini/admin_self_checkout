import 'package:admin_self_checkout/models/user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';

// If FirebaseAuth and/or GoogleSignIn are not injected into the UserRepository, then we instantiate them internally.
// This allows us to be able to inject mock instances so that we can easily test the UserRepository

class UserRepository {
  final FirebaseAuth _firebaseAuth;

  UserRepository({
    FirebaseAuth firebaseAuth,
  }) : _firebaseAuth = firebaseAuth ?? FirebaseAuth.instance;

  Future<void> signInWithCredentials(String email, String password) {
    return _firebaseAuth.signInWithEmailAndPassword(
      email: email,
      password: password,
    );
  }

  Future<void> signUp({String email, String password}) async {
    return await _firebaseAuth.createUserWithEmailAndPassword(
      email: email,
      password: password,
    );
  }

  Future<void> updateAuth({String name, String birthday}) async {
    var auth = _firebaseAuth.currentUser;
    await auth.updateProfile(displayName: name);
    final _firestore = FirebaseFirestore.instance;
    final userRef = await _firestore.collection("users").doc(auth.uid).get();

    await userRef.reference.update(
      {"displayName": name, "birthday": birthday},
    );
    return auth.reload();
  }

  Future<String> resetPassword(String email) async {
    try {
      await _firebaseAuth.sendPasswordResetEmail(email: email);
      return 'success';
    } catch (e) {
      //(e.message);
      return e.message;
    }
  }

  Future<void> signOut() async {
    return Future.wait([
      _firebaseAuth.signOut(),
    ]);
  }

  bool isSignedIn() {
    final currentUser = _firebaseAuth.currentUser;
    return currentUser != null;
  }

  User getCurrentUser() {
    return _firebaseAuth.currentUser;
  }

  Stream<UserModel> getUser(userId) {
    final FirebaseFirestore _firestore = FirebaseFirestore.instance;
    return _firestore
        .collection("users")
        .doc(userId)
        .snapshots()
        .map((doc) => UserModel.fromFirestore(doc));
  }

  Future<void> updateImage(UserModel user) async {
    final FirebaseFirestore _firestore = FirebaseFirestore.instance;

    await _firestore
        .collection("users")
        .doc(user.uid)
        .update({"avatarURL": user.avatarURL});
  }

  Future<void> updateUseDetails(UserModel user) async {
    final FirebaseFirestore _firestore = FirebaseFirestore.instance;

    await _firestore.collection("users").doc(user.uid).update({
      "displayName": user.displayName,
    });
  }

  ///
  /// Admin Related users repository
  /// Change this if you are setting this for the first time.
  /// TODO: (DEVELOPERS) - Write a security that allow listing by admin only
  ///

  /// Get all the users
  Stream<List<UserModel>> streamUsers() {
    try {
      final FirebaseFirestore _firestore = FirebaseFirestore.instance;
      return _firestore.collection("users").snapshots().map((list) =>
          list.docs.map((doc) => UserModel.fromFirestore(doc)).toList());
    } on PlatformException catch (e) {
      print(e.message);
      return null;
    }
  }
}
