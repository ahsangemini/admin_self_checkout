import 'package:admin_self_checkout/models/order.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:async';
import 'package:flutter/services.dart';

class OrdersRepository {
  final ordersCollection = FirebaseFirestore.instance.collection("orders");

  /// Get all the user Orderss
  Stream<List<OrderModel>> streamOrders(String userId) {
    print(userId);
    try {
      print("coming here");
      return ordersCollection
          .where("userId", isEqualTo: userId)
          .orderBy("createdAt", descending: true)
          .snapshots()
          .map((list) =>
              list.docs.map((doc) => OrderModel.fromFirestore(doc)).toList());
    } on PlatformException catch (e) {
      print(e.message);
      throw e.message;
    } catch (e) {
      print(e);
      throw e.message;
    }
  }

  /// Admin list orders
  ///
  ///

  Stream<List<OrderModel>> streamAdminOrders() {
    try {
      return ordersCollection
          .orderBy("createdAt", descending: true)
          .snapshots()
          .map((list) =>
              list.docs.map((doc) => OrderModel.fromFirestore(doc)).toList());
    } on PlatformException catch (e) {
      print(e.message);
      throw e.message;
    }
  }

  Future<void> changeStatus(OrderModel order, String type) {
    try {
      if (type == "dispatched") {
        return ordersCollection.doc(order.orderId).update(
          {"status2": type, "updatedAt": FieldValue.serverTimestamp()},
        );
      } else {
        return ordersCollection.doc(order.orderId).update(
          {"status3": type, "updatedAt": FieldValue.serverTimestamp()},
        );
      }
    } on PlatformException catch (e) {
      print(e.message);
      throw e.message;
    }
  }
}
