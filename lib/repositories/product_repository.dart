import 'dart:io';
import 'dart:typed_data';
import 'package:admin_self_checkout/models/product.dart';
import 'package:path/path.dart' as path;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'dart:async';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:uuid/uuid.dart';
import 'package:path/path.dart' as p;

class ProductRepository {
  final productCollection = FirebaseFirestore.instance.collection("products");

  /// Get all the Products
  Stream<List<ProductModel>> streamProducts() {
    try {
      return productCollection.where("active", isEqualTo: true).snapshots().map(
          (list) =>
              list.docs.map((doc) => ProductModel.fromFirestore(doc)).toList());
    } on PlatformException catch (e) {
      print(e.message);
      return null;
    }
  }

  ///
  /// SAVE IMAGES
  ///

  Future saveImage(PickedFile asset) async {
    var uuid = new Uuid();
    var imageName = uuid.v4();

    String path = asset.path;
    String _extension = p.extension(path).split('?').first;

    ByteData byteData = await asset
        .readAsBytes()
        .then((data) => ByteData.view(data as ByteBuffer));
    List<int> imageData = byteData.buffer.asUint8List();
    StorageReference ref = FirebaseStorage.instance
        .ref()
        .child('$imageName${_extension.toLowerCase()}');
    StorageUploadTask uploadTask = ref.putData(imageData);

    return await (await uploadTask.onComplete).ref.getDownloadURL();
  }

  ///
  ///
  ///   ADD Product TO FIRESTORE
  ///
  ///

  Future<void> addProduct(ProductModel product) async {
    try {
      File file = File(product.images.last);
      print(file.path);
      String _randomFileNameUUID = Uuid().v4();
      String storageUri =
          'products/$_randomFileNameUUID${path.extension(file.path)}';
      final StorageReference storageReference =
          FirebaseStorage().ref().child(storageUri);
      final StorageUploadTask uploadTask = storageReference.putFile(file);
      StorageTaskSnapshot storageTaskSnapshot;
      StorageTaskSnapshot snapshot = await uploadTask.onComplete.timeout(
          const Duration(seconds: 60),
          onTimeout: () =>
              throw ('Upload could not be completed. Operation timeout'));
      if (snapshot.error == null) {
        storageTaskSnapshot = snapshot;
        (storageTaskSnapshot.ref.getDownloadURL()).then((fileURL) async {
          await productCollection.add({
            ...product.toMap(),
            "images": FieldValue.arrayUnion([fileURL]),
            "createdAt": FieldValue.serverTimestamp(),
            "updatedAt": FieldValue.serverTimestamp()
          });
        });
      } else {
        //  Notifications.showSnackBar(context, "Sorry there was an error");
        throw ('An error occured while uploading image. Upload error');
      }
    } on PlatformException catch (e) {
      print(e.message);
      return null;
    }
  }

  Future<void> editProduct(ProductModel product, oldImages) async {
    try {
      List images = [];
      for (var item in product.images) {
        var url = await saveImage(item);
        images.add(url);
      }
      await productCollection.document(product.productId).updateData({
        ...product.toMap(),
        "images": [...images, ...oldImages],
        "updatedAt": FieldValue.serverTimestamp()
      });
    } on PlatformException catch (e) {
      print(e.message);
      return null;
    }
  }

  Future<void> deleteProduct(String productId) async {
    try {
      await productCollection.document(productId).updateData(
          {"active": false, "updatedAt": FieldValue.serverTimestamp()});
    } on PlatformException catch (e) {
      print(e.message);
      return null;
    }
  }
}
