import 'dart:io';
import 'dart:typed_data';
import 'package:admin_self_checkout/models/category.dart';
import 'package:path/path.dart' as path;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'dart:async';
import 'package:flutter/services.dart';
import 'package:uuid/uuid.dart';
import 'package:path/path.dart' as p;

class CategoryRepository {
  final categoryCollection =
      FirebaseFirestore.instance.collection("categories");

  /// Get all the Categorys
  Stream<List<CategoryModel>> streamCategorys() {
    try {
      return categoryCollection
          .where("active", isEqualTo: true)
          .snapshots()
          .map((list) => list.docs
              .map((doc) => CategoryModel.fromFirestore(doc))
              .toList());
    } on PlatformException catch (e) {
      print(e.message);
      return null;
    }
  }

  ///
  /// SAVE IMAGES
  ///

  Future saveImage(File asset) async {
    var uuid = new Uuid();
    var imageName = uuid.v4();

    String path = asset.path;
    String _extension = p.extension(path).split('?').first;

    ByteData byteData = await asset
        .readAsBytes()
        .then((data) => ByteData.view(data as ByteBuffer));
    List<int> imageData = byteData.buffer.asUint8List();
    StorageReference ref = FirebaseStorage.instance
        .ref()
        .child('$imageName${_extension.toLowerCase()}');
    StorageUploadTask uploadTask = ref.putData(imageData);

    return await (await uploadTask.onComplete).ref.getDownloadURL();
  }

  ///
  ///
  ///   ADD Category TO FIRESTORE
  ///
  ///
  Future<void> addCategory(CategoryModel category) async {
    File file = File(category.images);
    String _randomFileNameUUID = Uuid().v4();
    String storageUri =
        'categories/$_randomFileNameUUID${path.extension(file.path)}';
    final StorageReference storageReference =
        FirebaseStorage().ref().child(storageUri);
    final StorageUploadTask uploadTask = storageReference.putFile(file);
    StorageTaskSnapshot storageTaskSnapshot;
    StorageTaskSnapshot snapshot = await uploadTask.onComplete.timeout(
        const Duration(seconds: 60),
        onTimeout: () =>
            throw ('Upload could not be completed. Operation timeout'));
    if (snapshot.error == null) {
      storageTaskSnapshot = snapshot;
      (storageTaskSnapshot.ref.getDownloadURL()).then((fileURL) async {
        await categoryCollection.add({
          ...category.toMap(),
          "images": [fileURL],
          "createdAt": FieldValue.serverTimestamp(),
          "updatedAt": FieldValue.serverTimestamp()
        });
      });
    } else {
      //  Notifications.showSnackBar(context, "Sorry there was an error");
      throw ('An error occured while uploading image. Upload error');
    }
  }

  // Future<void> addCategory(CategoryModel category) async {
  //   try {
  //     var url = await saveImage(File(category.images));

  //     print(url);
  //     await categoryCollection.add({
  //       ...category.toMap(),
  //       "images": [url],
  //       "createdAt": FieldValue.serverTimestamp(),
  //       "updatedAt": FieldValue.serverTimestamp()
  //     });
  //   } on PlatformException catch (e) {
  //     print(e.message);
  //     return null;
  //   }
  // }

  Future<void> editCategory(CategoryModel category, oldImages) async {
    try {
      var url = await saveImage(File(category.images));

      await categoryCollection.doc(category.categoryId).update({
        ...category.toMap(),
        "images": oldImages.add(url),
        "createdAt": FieldValue.serverTimestamp(),
        "updatedAt": FieldValue.serverTimestamp()
      });
    } on PlatformException catch (e) {
      print(e.message);
      return null;
    }
  }

  Future<void> deleteCategory(String categoryId) async {
    try {
      await categoryCollection
          .doc(categoryId)
          .update({"active": false, "updatedAt": FieldValue.serverTimestamp()});
    } on PlatformException catch (e) {
      print(e.message);
      return null;
    }
  }
}
