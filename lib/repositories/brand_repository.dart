import 'dart:io';
import 'dart:typed_data';

import 'package:admin_self_checkout/models/brand.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'dart:async';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:uuid/uuid.dart';
import 'package:path/path.dart' as p;

class BrandRepository {
  final brandCollection = FirebaseFirestore.instance.collection("brands");

  /// Get all the brands
  Stream<List<BrandModel>> streamBrands() {
    try {
      return brandCollection.where("active", isEqualTo: true).snapshots().map(
          (list) =>
              list.docs.map((doc) => BrandModel.fromFirestore(doc)).toList());
    } on PlatformException catch (e) {
      print(e.message);
      return null;
    }
  }

  ///
  /// SAVE IMAGES
  ///

  Future saveImage(PickedFile asset) async {
    var uuid = new Uuid();
    var imageName = uuid.v4();

    String path = asset.path;
    String _extension = p.extension(path).split('?').first;

    ByteData byteData = await asset
        .readAsBytes()
        .then((data) => ByteData.view(data as ByteBuffer));
    List<int> imageData = byteData.buffer.asUint8List();
    StorageReference ref = FirebaseStorage.instance
        .ref()
        .child('$imageName${_extension.toLowerCase()}');
    StorageUploadTask uploadTask = ref.putData(imageData);

    return await (await uploadTask.onComplete).ref.getDownloadURL();
  }

  ///
  ///
  ///   ADD BRAND TO FIRESTORE
  ///
  ///

  Future<void> addBrand(BrandModel brand) async {
    try {
      File file = File(brand.images);
      String _randomFileNameUUID = Uuid().v4();
      String storageUri =
          'brands/$_randomFileNameUUID${p.extension(file.path)}';
      final StorageReference storageReference =
          FirebaseStorage().ref().child(storageUri);
      final StorageUploadTask uploadTask = storageReference.putFile(file);
      StorageTaskSnapshot storageTaskSnapshot;
      StorageTaskSnapshot snapshot = await uploadTask.onComplete.timeout(
          const Duration(seconds: 60),
          onTimeout: () =>
              throw ('Upload could not be completed. Operation timeout'));
      if (snapshot.error == null) {
        storageTaskSnapshot = snapshot;
        (storageTaskSnapshot.ref.getDownloadURL()).then((fileURL) async {
          await brandCollection.add({
            ...brand.toMap(),
            "images": FieldValue.arrayUnion([fileURL]),
            "createdAt": FieldValue.serverTimestamp(),
            "updatedAt": FieldValue.serverTimestamp()
          });
        });
      }
    } on PlatformException catch (e) {
      print(e.message);
      return null;
    }
  }

  Future<void> editBrand(BrandModel brand, oldImages) async {
    File file = File(brand.images);
    String _randomFileNameUUID = Uuid().v4();
    String storageUri =
        'categories/$_randomFileNameUUID${p.extension(file.path)}';
    final StorageReference storageReference =
        FirebaseStorage().ref().child(storageUri);
    final StorageUploadTask uploadTask = storageReference.putFile(file);
    StorageTaskSnapshot storageTaskSnapshot;
    StorageTaskSnapshot snapshot = await uploadTask.onComplete.timeout(
        const Duration(seconds: 60),
        onTimeout: () =>
            throw ('Upload could not be completed. Operation timeout'));
    if (snapshot.error == null) {
      storageTaskSnapshot = snapshot;
      (storageTaskSnapshot.ref.getDownloadURL()).then((fileURL) async {
        await brandCollection.document(brand.brandId).updateData({
          ...brand.toMap(),
          "images": FieldValue.arrayUnion(fileURL),
          "createdAt": FieldValue.serverTimestamp(),
          "updatedAt": FieldValue.serverTimestamp()
        });
      });
    }
  }

  Future<void> deleteBrand(String brandId) async {
    try {
      await brandCollection.document(brandId).updateData(
          {"active": false, "updatedAt": FieldValue.serverTimestamp()});
    } on PlatformException catch (e) {
      print(e.message);
      return null;
    }
  }
}
