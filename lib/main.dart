import 'dart:io';
import 'package:admin_self_checkout/bloc_delegate.dart';
import 'package:admin_self_checkout/screens/login/login.dart';
import 'package:admin_self_checkout/screens/signup/signup.dart';
import 'package:admin_self_checkout/screens/updateProfile/update_profile_page.dart';
import 'package:admin_self_checkout/services/authentication/authentication.dart';
import 'package:admin_self_checkout/services/authentication/bloc/authentication_bloc.dart';
import 'package:admin_self_checkout/utilities/style.dart';
import 'package:animated_splash/animated_splash.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

final RouteObserver<Route> routeObserver = RouteObserver<Route>();

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  BlocSupervisor.delegate = SimpleBlocDelegate();

  final AuthBase auth = Auth();
  runApp(
    Provider<AuthBase>(
      create: (_) => Auth(),
      child: (BlocProvider(
        create: (context) => AuthenticationBloc(auth: auth)..add(AppStarted()),
        child: MyApp(
          auth: auth,
        ),
      )),
    ),
  );
}

class MyApp extends StatelessWidget {
  final AuthBase auth;

  const MyApp({Key key, this.auth}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Admin Self-Checkout",
      theme: kTheme,
      debugShowCheckedModeBanner: false,
      home: BlocListener<AuthenticationBloc, AuthenticationState>(
        listener: (context, state) {
          // if (state is Authenticated) {
          //   Navigator.of(context).pushReplacement(
          //     Platform.isIOS
          //         ? CupertinoPageRoute(
          //             builder: (_) => BlocProvider.value(
          //                   value: BlocProvider.of<AuthenticationBloc>(context),
          //                   child: UpdateProfilePageContainer(user: state.user),
          //                 ))
          //         : MaterialPageRoute(
          //             builder: (_) => BlocProvider.value(
          //                   value: BlocProvider.of<AuthenticationBloc>(context),
          //                   child: UpdateProfilePageContainer(user: state.user),
          //                 )),
          //     // (Route<dynamic> route) => false
          //   );
          // }
          // else if (state is Unauthenticated) {
          //   Navigator.of(context).push(
          //     Platform.isIOS
          //         ? CupertinoPageRoute(
          //             builder: (_) => LoginPage(),
          //           )
          //         : MaterialPageRoute(
          //             builder: (_) => LoginPage(),
          //           ),
          //   );
          //}
          if (state is Unregistered) {
            Navigator.of(context).push(
              Platform.isIOS
                  ? CupertinoPageRoute(
                      builder: (_) => SignUp("logo"),
                    )
                  : MaterialPageRoute(
                      builder: (_) => SignUp("logo"),
                    ),
            );
          }
        },
        child: BlocBuilder<AuthenticationBloc, AuthenticationState>(
          builder: (context, state) {
            if (state is Uninitialized) {
              return SplashScreen();
            } else if (state is Unauthenticated) {
              return LoginPage();
              // Navigator.of(context).pushReplacement(
              //   Platform.isIOS
              //       ? CupertinoPageRoute(
              //           builder: (_) => BlocProvider.value(
              //             value: BlocProvider.of<AuthenticationBloc>(context),
              //             child: LoginPage(),
              //           ),
              //         )
              //       : MaterialPageRoute(
              //           builder: (_) => BlocProvider.value(
              //             value: BlocProvider.of<AuthenticationBloc>(context),
              //             child: LoginPage(),
              //           ),
              //         ),
              // );
            } else if (state is Authenticated) {
              return UpdateProfilePageContainer();
            }

            return Container(
              height: MediaQuery.of(context).size.height,
              color: Colors.red,
            );
          },
        ),
      ),
      routes: {
        '/settings': (context) => UpdateProfilePageContainer(),
      },
      onGenerateRoute: (RouteSettings settings) {
        if (Platform.isIOS) {
          switch (settings.name) {
            case '/':
              return CupertinoPageRoute(
                builder: (_) => SplashScreen(),
                settings: settings,
              );

            // case '/settings':
            //   return CupertinoPageRoute(
            //     builder: (_) => UpdateProfilePageContainer(stat),
            //     settings: settings,
            //   );
            // case '/addProduct':
            //   return CupertinoPageRoute(
            //     builder: (_) => AddNewProductPageContainer(),
            //     settings: settings,
            //   );

            default:
              return CupertinoPageRoute(
                builder: (_) => SplashScreen(),
                settings: settings,
              );
          }
        } else {
          switch (settings.name) {
            case '/':
              return MaterialPageRoute(
                builder: (_) => SplashScreen(),
                settings: settings,
              );

            // case '/settings':
            //   return MaterialPageRoute(
            //     builder: (_) => UpdateProfilePageContainer(),
            //     settings: settings,
            // //   );
            // case '/addProduct':
            //   return MaterialPageRoute(
            //     builder: (_) => AddNewProductPageContainer(),
            //     settings: settings,
            //   );

            default:
              return MaterialPageRoute(
                builder: (_) => SplashScreen(),
                settings: settings,
              );
          }
        }
      },
    );
  }
}

class SplashScreen extends StatelessWidget {
  SplashScreen({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return AnimatedSplash(
      imagePath: 'assets/icons/amazon.svg',
      home: LoginPage(),
      duration: 2500,
      type: AnimatedSplashType.StaticDuration,
    );
  }
}
