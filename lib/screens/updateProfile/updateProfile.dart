import 'package:admin_self_checkout/elements/button.dart';
import 'package:admin_self_checkout/elements/imageField.dart';
import 'package:admin_self_checkout/elements/loadingDialog.dart';
import 'package:admin_self_checkout/elements/notification.dart';
import 'package:admin_self_checkout/elements/simpleCard.dart';
import 'package:admin_self_checkout/screens/updateProfile/passwordExpanded/bloc/passwordexpanded_bloc.dart';
import 'package:admin_self_checkout/screens/updateProfile/profileInitial.dart';
import 'package:admin_self_checkout/screens/updateProfile/profilebloc/bloc/profilebloc_bloc.dart';
import 'package:admin_self_checkout/screens/updateProfile/updatebloc.dart';
import 'package:admin_self_checkout/services/authentication/authentication.dart';
import 'package:admin_self_checkout/utilities/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class Profile extends StatefulWidget {
  Profile({Key key}) : super(key: key);
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  AuthBase authBase;
  @override
  void initState() {
    authBase = Provider.of<AuthBase>(context, listen: false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: <Widget>[
        SliverFillRemaining(
          child: BlocProvider<ProfileblocBloc>(
            create: (context) => ProfileblocBloc(authBase)..add(ProfileShown()),
            child: BlocListener<ProfileblocBloc, ProfileblocState>(
              listener: (context, state) {
                if (state is ErrorState) {
                  Notifications.showSnackBarWithError(context, state.message);
                }
              },
              child: BlocBuilder<ProfileblocBloc, ProfileblocState>(
                builder: (context, state) {
                  if (state is ProfileblocInitial) {
                    return Container();
                  } else if (state is ProfileblocUser) {
                    return Center(
                      child: ProfileInitial(
                        user: state.user,
                      ),
                    );
                  } else if (state is EditingProfile) {
                    return EditForm(state.user);
                  } else if (state is SavedState) {
                    return ProfileInitial(
                      user: state.user,
                    );
                    // Container(
                    //   height: 200,
                    //   color: Colors.red,
                    // );
                  }

                  return Container();
                },
              ),
            ),
          ),
        )
      ],
    );
  }
}

class EditForm extends StatelessWidget {
  EditForm(
    this.user, {
    Key key,
  }) : super(key: key);
  final User user;
  AuthBase authBase;

  Widget build(BuildContext context) {
    Widget _labelText(
        IconData leading, String fieldLabel, BuildContext context) {
      return Row(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: Icon(
              leading,
              color: AppColors.kYellow,
              size: 12,
            ),
          ),
          Text(fieldLabel,
              style: Theme.of(context)
                  .textTheme
                  .caption
                  .copyWith(color: AppColors.kGrey.withOpacity(0.4))),
        ],
      );
    }

    Widget _showFullNameInput(BuildContext context, UpdateFromField update) {
      return TextFieldBlocBuilder(
        textFieldBloc: update.nameField,
        style: Theme.of(context).textTheme.subhead,
      );
    }

    Widget _showEmailInput(BuildContext context, UpdateFromField update) {
      return TextFieldBlocBuilder(
        style: Theme.of(context).textTheme.subhead,
        textFieldBloc: update.emailField,
      );
    }

    Widget _showPasswordReset(Function onPressed) {
      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "••••••••",
                  style: Theme.of(context).textTheme.subhead,
                ),
                FlatButton(
                  padding: EdgeInsets.all(0),
                  onPressed: onPressed,
                  child: Text(
                    "Reset Password",
                    textAlign: TextAlign.right,
                    style: Theme.of(context)
                        .textTheme
                        .button
                        .copyWith(color: AppColors.kYellow),
                  ),
                ),
              ],
            ),
            Divider(
              color: AppColors.kGrey,
            ),
          ],
        ),
      );
    }

    Widget _showPasswordInput(UpdateFromField formbloc) {
      return IntrinsicHeight(
        child: TextFieldBlocBuilder(
          textFieldBloc: formbloc.password,
          style: Theme.of(context).textTheme.subhead,
          suffixButton: SuffixButton.obscureText,
          decoration: InputDecoration(
            hintText: '••••••',
          ),
        ),
      );
    }

    Widget _showConfirmPasswordInput(UpdateFromField formbloc) {
      return IntrinsicHeight(
        child: TextFieldBlocBuilder(
          textFieldBloc: formbloc.confirmPassword,
          style: Theme.of(context).textTheme.subhead,
          suffixButton: SuffixButton.obscureText,
          decoration: InputDecoration(
            hintText: '••••••',
          ),
        ),
      );
    }

    return BlocProvider(
      create: (context) =>
          UpdateFromField(Provider.of<AuthBase>(context, listen: false)),
      child: Builder(builder: (context) {
        final _fromField = BlocProvider.of<UpdateFromField>(context);
        return FormBlocListener<UpdateFromField, String, String>(
          onSubmitting: (context, state) => LoadingDialog.show(context),
          onSuccess: (context, state) {
            LoadingDialog.hide(context);
            BlocProvider.of<ProfileblocBloc>(context)..add(SaveProfile());
          },
          onFailure: (context, state) {
            LoadingDialog.hide(context);
            Notifications.showSnackBarWithError(context, state.failureResponse);
          },
          child: ListView(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(bottom: 100),
                    child: SimpleCard(
                      color: AppColors.kLightGrey.withOpacity(0.1),
                      width: deviceWidth(context) * 0.85,
                      child: Column(children: <Widget>[
                        Container(
                          height: 60,
                        ),
                        ImageFieldBlocBuilder(
                          fileFieldBloc: _fromField.imageField,
                          url: user.url,
                        ),
                        Column(
                          children: <Widget>[
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 30.0),
                              child: Column(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(top: 24.0),
                                    child: _labelText(
                                        FontAwesomeIcons.solidUser,
                                        "Full Name",
                                        context),
                                  ),
                                  _showFullNameInput(context, _fromField),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 24.0),
                                    child: _labelText(FontAwesomeIcons.at,
                                        "Email Address", context),
                                  ),
                                  _showEmailInput(context, _fromField),
                                  SizedBox(
                                    height: 30,
                                  ),
                                  Padding(
                                      padding: const EdgeInsets.only(top: 24.0),
                                      child: _labelText(FontAwesomeIcons.lock,
                                          "Password", context)),
                                  //_showPasswordInput(_fromField),
                                  BlocProvider<PasswordexpandedBloc>(
                                    create: (context) => PasswordexpandedBloc()
                                      ..add(PasswordInitial()),
                                    child: BlocBuilder<PasswordexpandedBloc,
                                        PasswordexpandedState>(
                                      builder: (context, state) {
                                        if (state is PasswordInitial) {
                                          _showPasswordReset(() => BlocProvider
                                              .of<PasswordexpandedBloc>(context)
                                            ..add(ShowPassword()));
                                        } else if (state is PasswordExpand) {
                                          return Column(
                                            children: <Widget>[
                                              _showPasswordInput(_fromField),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 24.0),
                                                child: _labelText(
                                                    FontAwesomeIcons.lock,
                                                    "Confirm Password",
                                                    context),
                                              ),
                                              _showConfirmPasswordInput(
                                                  _fromField),
                                            ],
                                          );
                                        } else {
                                          return _showPasswordReset(() =>
                                              BlocProvider.of<
                                                  PasswordexpandedBloc>(context)
                                                ..add(ShowPassword()));
                                        }
                                      },
                                    ),
                                  ),

                                  Padding(
                                    padding: const EdgeInsets.only(
                                        bottom: 20, top: 25),
                                    child: Button(
                                      height: 42,
                                      width: 0.25,
                                      onPressed: () => _fromField.submit(),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                right: 8.0),
                                            child: Icon(
                                              FontAwesomeIcons.edit,
                                              color: AppColors.kWhite,
                                              size: 20,
                                            ),
                                          ),
                                          Text(
                                            'Save',
                                            style: Theme.of(context)
                                                .textTheme
                                                .button
                                                .copyWith(
                                                    color: AppColors.kWhite),
                                            textAlign: TextAlign.center,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ]),
                    ),
                  )
                ],
              ),
            ],
          ),
        );
      }),
    );
  }
}
