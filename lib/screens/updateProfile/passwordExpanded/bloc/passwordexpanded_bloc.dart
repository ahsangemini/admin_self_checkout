import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'passwordexpanded_event.dart';
part 'passwordexpanded_state.dart';

class PasswordexpandedBloc
    extends Bloc<PasswordexpandedEvent, PasswordexpandedState> {
  @override
  PasswordexpandedState get initialState => PasswordexpandedInitial();

  @override
  Stream<PasswordexpandedState> mapEventToState(
    PasswordexpandedEvent event,
  ) async* {
    if (event is PasswordInitial) {
      _mapPasswordInitialtoState();
    } else if (event is ShowPassword) {
      yield* _mapShowPasswordtoState();
    }
  }

  Stream<PasswordexpandedState> _mapPasswordInitialtoState() async* {
    yield PasswordexpandedInitial();
  }

  Stream<PasswordexpandedState> _mapShowPasswordtoState() async* {
    yield PasswordExpand();
  }
}
