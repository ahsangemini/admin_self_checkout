part of 'passwordexpanded_bloc.dart';

@immutable
abstract class PasswordexpandedState {}

class PasswordexpandedInitial extends PasswordexpandedState {}

class PasswordExpand extends PasswordexpandedState {}
