part of 'passwordexpanded_bloc.dart';

@immutable
abstract class PasswordexpandedEvent {}

class PasswordInitial extends PasswordexpandedEvent {}

class ShowPassword extends PasswordexpandedEvent {}
