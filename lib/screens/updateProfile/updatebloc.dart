import 'dart:io';
import 'package:admin_self_checkout/services/authentication/authentication.dart'
    as authbloc;
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:form_bloc/form_bloc.dart';
import 'package:provider/provider.dart';

class UpdateFromField extends FormBloc<String, String> {
  UpdateFromField(this.auth) {
    confirmPassword.subscribeToFieldBlocs([password]);
    confirmPassword
        .addValidators([FieldBlocValidators.confirmPassword(password)]);
  }

  FirebaseStorage _storage = FirebaseStorage.instance;

  final authbloc.AuthBase auth;
  final nameField = TextFieldBloc(
    validators: [FieldBlocValidators.requiredTextFieldBloc],
  );
  final emailField = TextFieldBloc(
    validators: [FieldBlocValidators.email],
  );

  final imageField = InputFieldBloc<File>();

  final password = TextFieldBloc(
    validators: [FieldBlocValidators.passwordMin6Chars],
  );

  final confirmPassword = TextFieldBloc();

  @override
  List<FieldBloc> get fieldBlocs => [
        nameField,
        emailField,
        imageField,
      ];
  void prefillFields() async {
    authbloc.User user = auth.currentUser();
    nameField.updateInitialValue(user.name);
    emailField.updateInitialValue(user.email);
  }

  @override
  Stream<FormBlocState<String, String>> onSubmitting() async* {
    try {
      String url;
      authbloc.User user = auth.currentUser();

      if (imageField.value != null) {
        var fileName = imageField.value;
        StorageReference reference = _storage.ref().child("images/${user.uid}");
        StorageUploadTask uploadTask = reference.putFile(fileName);
        var dowurl = await (await uploadTask.onComplete).ref.getDownloadURL();
        url = dowurl.toString();
      }
      if (nameField.value.isNotEmpty) {
        await auth.updateUserInfo(nameField.value, url);
      }
      if (emailField.value.isNotEmpty) {
        await auth.updateEmail(emailField.value);
      }
      if (password.value.isNotEmpty) {
        await auth.updatePassword(password.value);
      }
      yield state.toSuccess();
    } catch (e) {
      yield state.toFailure();
    }
  }
}
