part of 'profilebloc_bloc.dart';

@immutable
abstract class ProfileblocState {}

class ProfileblocInitial extends ProfileblocState {}

class ProfileblocUser extends ProfileblocState {
  final User user;

  ProfileblocUser(this.user);
}

class ProfilePage extends ProfileblocState {}

class EditingProfile extends ProfileblocState {
  final User user;

  EditingProfile(this.user);
}

class ErrorState extends ProfileblocState {
  final String message;

  ErrorState(this.message);
}

class SavedState extends ProfileblocState {
  final User user;

  SavedState(this.user);
}

class PasswordExpanded extends ProfileblocState {}
