import 'dart:async';

import 'package:admin_self_checkout/services/authentication/authentication.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import 'package:provider/provider.dart';

part 'profilebloc_event.dart';
part 'profilebloc_state.dart';

class ProfileblocBloc extends Bloc<ProfileblocEvent, ProfileblocState> {
  ProfileblocBloc(this.auth);
  final AuthBase auth;

  @override
  ProfileblocState get initialState => ProfileblocInitial();

  @override
  Stream<ProfileblocState> mapEventToState(
    ProfileblocEvent event,
  ) async* {
    if (event is ProfileShown) {
      yield* _mapShowProfiletoState();
    } else if (event is EditProfile) {
      yield* _mapEditProfiletoState();
    } else if (event is SaveProfile) {
      yield* _mapSaveProfiletoState();
    }
  }

  Stream<ProfileblocState> _mapShowProfiletoState() async* {
    try {
      if (await auth.isSignedIn()) {
        yield ProfileblocUser(await auth.currentUser());
      } else {
        yield ErrorState("User not signed in");
      }
    } catch (_) {
      yield ErrorState("Unknown Error Occured");
    }
  }

  Stream<ProfileblocState> _mapEditProfiletoState() async* {
    try {
      if (await auth.isSignedIn()) {
        yield EditingProfile(await auth.currentUser());
      } else {
        yield ErrorState("User not signed in");
      }
    } catch (_) {
      yield ErrorState("Unknown Error Occured");
    }
  }

  Stream<ProfileblocState> _mapSaveProfiletoState() async* {
    try {
      if (await auth.isSignedIn()) {
        yield SavedState(await auth.currentUser());
      } else {
        yield ErrorState("User not signed in");
      }
    } catch (_) {
      yield ErrorState("Unknown Error Occured");
    }
  }
}
