part of 'profilebloc_bloc.dart';

@immutable
abstract class ProfileblocEvent {}

class ProfileShown extends ProfileblocEvent {}

class EditProfile extends ProfileblocEvent {}

class SaveProfile extends ProfileblocEvent {}

class ShowProfile extends ProfileblocEvent {}
