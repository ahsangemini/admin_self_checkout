import 'package:admin_self_checkout/admin/widgets/drawer.dart';
import 'package:admin_self_checkout/elements/pagecontainerbase.dart';
import 'package:admin_self_checkout/screens/updateProfile/UpdateProfile.dart';
import 'package:admin_self_checkout/services/authentication/authentication.dart';
import 'package:admin_self_checkout/utilities/style.dart';
import 'package:flutter/material.dart';

class UpdateProfilePageContainer extends PageContainerBase {
  final User user;

  UpdateProfilePageContainer({this.user});
  @override
  Widget get body => Profile();

  @override
  String get pageTitle => "Settings";

  @override
  Widget get background => Container();

  @override
  Color get backgroundColor => AppColors.kLighterYellow;

  @override
  Widget get menuDrawer => CustomDrawer();

  @override
  // TODO: implement floatingactionbutton
  Widget get floatingactionbutton => null;
}
