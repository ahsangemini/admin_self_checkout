import 'package:admin_self_checkout/elements/button.dart';
import 'package:admin_self_checkout/elements/simpleCard.dart';
import 'package:admin_self_checkout/screens/updateProfile/profilebloc/bloc/profilebloc_bloc.dart';
import 'package:admin_self_checkout/services/authentication/authentication.dart';
import 'package:admin_self_checkout/utilities/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:provider/provider.dart';

class ProfileInitial extends StatelessWidget {
  ProfileInitial({Key key, @required this.user}) : super(key: key);
  final User user;

  AuthBase authBase;

  Widget _showFullName(String name, BuildContext context) {
    return Text(
      name,
      style: Theme.of(context).textTheme.subhead,
      textAlign: TextAlign.left,
    );
  }

  Widget _showEmail(String email, BuildContext context) {
    return Text(
      email,
      style: Theme.of(context).textTheme.subhead,
      textAlign: TextAlign.left,
    );
  }

  // Widget _showPasswordInput() {
  //   return IntrinsicHeight(
  //     child: TextFormField(
  //       onSaved: (val) => _password = val,
  //       validator: (val) => val.length < 6 ? 'Password too short' : null,
  //       style: Theme.of(context).textTheme.caption,
  //       obscureText: _obscureText,
  //       decoration: InputDecoration(
  //         suffixIcon: FlatButton(
  //           padding: EdgeInsets.all(0),
  //           onPressed: () {},
  //           child: Text(
  //             "Reset Password",
  //             textAlign: TextAlign.right,
  //             style: Theme.of(context).textTheme.button.copyWith(color: myBlue),
  //           ),
  //         ),
  //         // GestureDetector(
  //         //   onTap: () {
  //         //     setState(() => _obscureText = !_obscureText);
  //         //   },
  //         //   child: Icon(
  //         //     _obscureText ? Icons.visibility : Icons.visibility_off,
  //         //     color: Colors.amber,
  //         //     size: 18,
  //         //   ),
  //         // ),
  //         hintText: '••••••',
  //       ),
  //     ),
  //   );
  // }

  Widget _labelText(IconData leading, String fieldLabel, BuildContext context) {
    return Row(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(right: 8.0),
          child: Icon(
            leading,
            color: AppColors.kYellow,
            size: 12,
          ),
        ),
        Text(fieldLabel,
            style: Theme.of(context)
                .textTheme
                .caption
                .copyWith(color: AppColors.kGrey.withOpacity(0.4))),
      ],
    );
  }

  // void _submit() {
  //   final form = _formKey.currentState;

  //   if (form.validate()) {
  //     form.save();
  //     print('Email: $_email, Password: $_password');
  //   }
  //   if (_edit) {
  //     setState(() {
  //       _edit = false;
  //     });
  //   } else {
  //     setState(() {
  //       _edit = true;
  //     });
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Stack(
          alignment: Alignment.topCenter,
          children: <Widget>[
            Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 50.0, bottom: 100),
                  child: SimpleCard(
                    color: AppColors.kGrey.withOpacity(0.1),
                    width: deviceWidth(context) * 0.85,
                    child: Column(
                      children: <Widget>[
                        Container(
                          height: 60,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10),
                          child: Text(
                            "Profile Image",
                            style: Theme.of(context).textTheme.subhead.copyWith(
                                  color: AppColors.kGrey.withOpacity(0.4),
                                ),
                          ),
                        ),
                        Column(
                          children: <Widget>[
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 30.0),
                              child: Column(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(top: 24.0),
                                    child: _labelText(
                                        FontAwesomeIcons.solidUser,
                                        "Full Name",
                                        context),
                                  ),
                                  _showFullName(user.name.toString(), context),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 24.0),
                                    child: _labelText(FontAwesomeIcons.at,
                                        "Email Address", context),
                                  ),
                                  _showEmail(user.email.toString(), context),
                                  // Padding(
                                  //   padding:
                                  //       const EdgeInsets.only(top: 24.0),
                                  //   child: _labelText(
                                  //       FontAwesomeIcons.lock,
                                  //       "Password"),
                                  // ),
                                  // _showPasswordInput(),
                                ],
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(bottom: 20, top: 25),
                              child: Button(
                                height: 42,
                                width: 0.25,
                                onPressed: () =>
                                    BlocProvider.of<ProfileblocBloc>(context)
                                      ..add(EditProfile()),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Padding(
                                      padding:
                                          const EdgeInsets.only(right: 8.0),
                                      child: Icon(
                                        FontAwesomeIcons.edit,
                                        color: AppColors.kWhite,
                                        size: 20,
                                      ),
                                    ),
                                    Text(
                                      "Edit",
                                      style: Theme.of(context)
                                          .textTheme
                                          .button
                                          .copyWith(color: AppColors.kWhite),
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
            CircleAvatar(
              radius: 55,
              backgroundImage: user.url != null
                  ? NetworkImage(user.url)
                  : AssetImage(
                      'assets/images/photo.png',
                    ),
            ),
          ],
        ),
      ],
    );
  }
}
