import 'dart:io';

import 'package:admin_self_checkout/elements/background.dart';
import 'package:admin_self_checkout/elements/button.dart';
import 'package:admin_self_checkout/elements/loadingDialog.dart';
import 'package:admin_self_checkout/elements/logo.dart';
import 'package:admin_self_checkout/elements/notification.dart';
import 'package:admin_self_checkout/screens/login/login_bloc.dart';
import 'package:admin_self_checkout/screens/signup/signup.dart';
import 'package:admin_self_checkout/services/authentication/authentication.dart';
import 'package:admin_self_checkout/services/authentication/bloc/authentication_bloc.dart';
import 'package:admin_self_checkout/utilities/style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key key}) : super(key: key);
  @override
  LoginPageState createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();
  final String logo = "logo";

  Widget _showEmailInput(LoginFormBloc formbloc, BuildContext context) {
    return TextFieldBlocBuilder(
      textFieldBloc: formbloc.emailField,
      style: Theme.of(context).textTheme.subhead,
      decoration: InputDecoration(
        hintText: 'mrahsanali5@gmail.com',
      ),
    );
  }

  Widget _showPasswordInput(LoginFormBloc formbloc, BuildContext context) {
    return IntrinsicHeight(
      child: TextFieldBlocBuilder(
        textFieldBloc: formbloc.passwordField,
        style: Theme.of(context).textTheme.subhead,
        suffixButton: SuffixButton.obscureText,
        decoration: InputDecoration(
          hintText: '••••••',
        ),
      ),
    );
  }

  Widget _labelText(IconData leading, String fieldLabel, BuildContext context) {
    return Row(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(right: 8.0),
          child: Icon(
            leading,
            color: AppColors.kLightYellow,
            size: 12,
          ),
        ),
        Text(fieldLabel,
            style: Theme.of(context)
                .textTheme
                .caption
                .copyWith(color: AppColors.kGrey.withOpacity(0.4))),
      ],
    );
  }

  void _submit(LoginFormBloc formbloc) {
    final form = _formKey.currentState;
    formbloc.submit;
  }

  @override
  Widget build(BuildContext context) {
    final AuthBase auth = Provider.of<AuthBase>(context);

    return BlocProvider<LoginFormBloc>(
      create: (context) => LoginFormBloc(auth),
      child: Builder(builder: (context) {
        final _formbloc = BlocProvider.of<LoginFormBloc>(context);
        return Scaffold(
          body: FormBlocListener<LoginFormBloc, String, String>(
            onSubmitting: (context, state) => LoadingDialog.show(context),
            onSuccess: (context, state) {
              LoadingDialog.hide(context);
              BlocProvider.of<AuthenticationBloc>(context)
                ..add(
                  LoggedIn(),
                );
            },
            onFailure: (context, state) {
              LoadingDialog.hide(context);
              Notifications.showSnackBarWithError(
                  context, state.failureResponse);
            },
            child: Container(
              child: Center(
                child: SingleChildScrollView(
                  child: Form(
                    /* */
                    key: _formKey,
                    child: Column(
                      children: [
                        SizedBox(height: deviceHeight(context) * 0.15),
                        Hero(tag: logo, child: Logo(0.6506)),
                        SizedBox(height: deviceHeight(context) * 0.18),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 45.0),
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0),
                                child: _labelText(
                                    FontAwesomeIcons.at, "Username", context),
                              ),
                              _showEmailInput(_formbloc, context),
                              Padding(
                                padding: const EdgeInsets.only(top: 24.0),
                                child: _labelText(
                                    FontAwesomeIcons.lock, "Password", context),
                              ),
                              _showPasswordInput(_formbloc, context),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 20, top: 25),
                          child: Button(
                            height: 42,
                            width: 0.80,
                            onPressed: () => _formbloc.submit(),
                            child: Text(
                              'Submit',
                              style: Theme.of(context)
                                  .textTheme
                                  .subhead
                                  .copyWith(color: AppColors.kWhite),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                        Text(
                          "Don't have an account?",
                          style: Theme.of(context).textTheme.subtitle.copyWith(
                                color: AppColors.kGrey,
                              ),
                        ),
                        FlatButton(
                          child: Text(
                            'Sign Up Now',
                            style: Theme.of(context)
                                .textTheme
                                .subtitle
                                .copyWith(color: AppColors.kYellow),
                          ),
                          onPressed: () =>
                              // BlocProvider.of<AuthenticationBloc>(context)
                              //   ..add(NewRegister()),

                              Navigator.of(context).push(
                            Platform.isIOS
                                ? CupertinoPageRoute(
                                    builder: (_) => BlocProvider.value(
                                      value:
                                          BlocProvider.of<AuthenticationBloc>(
                                              context),
                                      child: SignUp("logo"),
                                    ),
                                  )
                                : MaterialPageRoute(
                                    builder: (_) => BlocProvider.value(
                                      value:
                                          BlocProvider.of<AuthenticationBloc>(
                                              context),
                                      child: SignUp("logo"),
                                    ),
                                  ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      }),
    );
  }
}
