import 'package:admin_self_checkout/services/authentication/authentication.dart';
import 'package:form_bloc/form_bloc.dart';

class LoginFormBloc extends FormBloc<String, String> {
  LoginFormBloc(this.auth);
  final emailField = TextFieldBloc(
    validators: [FieldBlocValidators.email],
  );

  AuthBase auth;
  final passwordField = TextFieldBloc(
    validators: [FieldBlocValidators.requiredInputFieldBloc],
  );

  @override
  List<FieldBloc> get fieldBlocs => [
        emailField,
        passwordField,
      ];

  @override
  Stream<FormBlocState<String, String>> onSubmitting() async* {
    try {
      await auth.signInWithEmailAndPassword(
          emailField.value, passwordField.value);
      yield state.toSuccess();
    } catch (e) {
      yield state.toFailure();
    }
  }
}
