import 'package:admin_self_checkout/elements/background.dart';
import 'package:admin_self_checkout/elements/button.dart';
import 'package:admin_self_checkout/elements/imageField.dart';
import 'package:admin_self_checkout/elements/loadingDialog.dart';
import 'package:admin_self_checkout/elements/logo.dart';
import 'package:admin_self_checkout/elements/notification.dart';
import 'package:admin_self_checkout/screens/signup/signup_bloc.dart';
import 'package:admin_self_checkout/services/authentication/authentication.dart';
import 'package:admin_self_checkout/services/authentication/bloc/authentication_bloc.dart';
import 'package:admin_self_checkout/utilities/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class SignUp extends StatefulWidget {
  SignUp(this.logo, {Key key}) : super(key: key);
  final String logo;

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  String _fullname, _email, _password;

  // @override
  // void initState() {
  //   super.initState();
  // }

  Widget _showFullNameInput(SignupFormBloc formbloc) {
    return TextFieldBlocBuilder(
        textFieldBloc: formbloc.nameField,
        style: Theme.of(context).textTheme.subhead,
        decoration: InputDecoration(
          hintText: 'Ahsan Ali',
        ));
  }

  Widget _showEmailInput(SignupFormBloc formbloc) {
    return TextFieldBlocBuilder(
      textFieldBloc: formbloc.email,
      style: Theme.of(context).textTheme.subhead,
      decoration: InputDecoration(
        hintText: 'mrahsanali5@email.com',
      ),
    );
  }

  Widget _showPasswordInput(SignupFormBloc formbloc) {
    return IntrinsicHeight(
      child: TextFieldBlocBuilder(
        textFieldBloc: formbloc.password,
        style: Theme.of(context).textTheme.subhead,
        suffixButton: SuffixButton.obscureText,
        decoration: InputDecoration(
          hintText: '••••••',
        ),
      ),
    );
  }

  Widget _showConfirmPasswordInput(SignupFormBloc formbloc) {
    return IntrinsicHeight(
      child: TextFieldBlocBuilder(
        textFieldBloc: formbloc.confirmPassword,
        style: Theme.of(context).textTheme.subhead,
        suffixButton: SuffixButton.obscureText,
        decoration: InputDecoration(
          hintText: '••••••',
        ),
      ),
    );
  }

  Widget _labelText(IconData leading, String fieldLabel) {
    return Row(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(right: 8.0),
          child: Icon(
            leading,
            color: AppColors.kYellow,
            size: 12,
          ),
        ),
        Text(fieldLabel,
            style: Theme.of(context)
                .textTheme
                .subhead
                .copyWith(color: AppColors.kGrey.withOpacity(0.7))),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<SignupFormBloc>(
      create: (context) =>
          SignupFormBloc(Provider.of<AuthBase>(context, listen: false)),
      child: Builder(
        builder: (context) {
          final _formBloc = BlocProvider.of<SignupFormBloc>(context);
          return Scaffold(
            body: FormBlocListener<SignupFormBloc, String, String>(
              onSubmitting: (context, state) => LoadingDialog.show(context),
              onSuccess: (context, state) {
                Navigator.pop(context);

                LoadingDialog.hide(context);
                // BlocProvider.of<AuthenticationBloc>(context)
                //   ..add(
                //     Registered(),
                //   );
              },
              onFailure: (context, state) {
                LoadingDialog.hide(context);
                Notifications.showSnackBarWithError(
                    context, state.failureResponse);
              },
              child: Container(
                child: Center(
                  child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Container(
                              padding: const EdgeInsets.only(
                                  top: 35, right: 16, bottom: 12),
                              alignment: Alignment.topRight,
                              child: Hero(
                                tag: widget.logo,
                                child: Logo(0.21),
                              ),
                            ),
                          ],
                        ),
                        ImageFieldBlocBuilder(
                          fileFieldBloc: _formBloc.imageField,
                        ),
                        Column(
                          children: <Widget>[
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 45.0),
                              child: Column(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(top: 24.0),
                                    child: _labelText(
                                        FontAwesomeIcons.solidUser,
                                        "Full Name"),
                                  ),
                                  _showFullNameInput(_formBloc),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 24.0),
                                    child: _labelText(
                                        FontAwesomeIcons.at, "Email Address"),
                                  ),
                                  _showEmailInput(_formBloc),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 24.0),
                                    child: _labelText(
                                        FontAwesomeIcons.lock, "Password"),
                                  ),
                                  _showPasswordInput(_formBloc),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 24.0),
                                    child: _labelText(FontAwesomeIcons.lock,
                                        "Confirm Password"),
                                  ),
                                  _showConfirmPasswordInput(_formBloc),
                                ],
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(bottom: 20, top: 25),
                              child: Button(
                                height: 42,
                                width: 0.80,
                                onPressed: () => _formBloc.submit(),
                                child: Text(
                                  'Submit',
                                  style: Theme.of(context)
                                      .textTheme
                                      .subhead
                                      .copyWith(color: AppColors.kWhite),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                            Text("Already have an account?",
                                style: Theme.of(context)
                                    .textTheme
                                    .subtitle
                                    .copyWith(color: AppColors.kGrey)),
                            FlatButton(
                                child: Text('Login Now',
                                    style: Theme.of(context)
                                        .textTheme
                                        .subtitle
                                        .copyWith(color: AppColors.kYellow)),
                                onPressed: () => {
                                      BlocProvider.of<AuthenticationBloc>(
                                          context)
                                        ..add(
                                          LoggedOut(),
                                        ),
                                      Navigator.of(context).pop(),
                                    }),
                            SizedBox(
                              height: 20,
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
