import 'dart:io';

import 'package:admin_self_checkout/services/authentication/authentication.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:form_bloc/form_bloc.dart';

class SignupFormBloc extends FormBloc<String, String> {
  AuthBase auth;
  FirebaseStorage _storage = FirebaseStorage.instance;

  final imageField = InputFieldBloc<File>();

  final nameField = TextFieldBloc(
    validators: [FieldBlocValidators.requiredTextFieldBloc],
  );
  final email = TextFieldBloc(
    validators: [FieldBlocValidators.email],
  );

  final password = TextFieldBloc(
    validators: [FieldBlocValidators.passwordMin6Chars],
  );

  final confirmPassword = TextFieldBloc();

  SignupFormBloc(this.auth) {
    confirmPassword.subscribeToFieldBlocs([password]);
    confirmPassword
        .addValidators([FieldBlocValidators.confirmPassword(password)]);
  }
  @override
  List<FieldBloc> get fieldBlocs =>
      [email, password, confirmPassword, imageField, nameField];

  @override
  Stream<FormBlocState<String, String>> onSubmitting() async* {
    try {
      String url;

      await auth.createUserWithEmailAndPassword(email.value, password.value);
      await auth.signInWithEmailAndPassword(email.value, password.value);
      if (imageField.value != null) {
        var fileName = imageField.value;
        var user = await auth.currentUser();
        StorageReference reference = _storage.ref().child("images/${user.uid}");
        StorageUploadTask uploadTask = reference.putFile(fileName);
        var dowurl = await (await uploadTask.onComplete).ref.getDownloadURL();
        url = dowurl.toString();
      }
      await auth.updateUserInfo(nameField.value, url);
      await auth.signOut();

      yield state.toSuccess();
    } catch (e) {
      var result = await auth.isSignedIn();
      if (result) {
        auth.signOut();
      }
      yield state.toFailure();
    }
  }
}
