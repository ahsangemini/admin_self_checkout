import 'dart:async';

import 'package:admin_self_checkout/services/authentication/authentication.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'authentication_event.dart';
part 'authentication_state.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final AuthBase _auth;
  AuthenticationBloc({@required AuthBase auth})
      : assert(auth != null),
        _auth = auth;
  @override
  AuthenticationState get initialState => Uninitialized();
  @override
  Stream<AuthenticationState> mapEventToState(
    AuthenticationEvent event,
  ) async* {
    if (event is AppStarted) {
      yield* _mapAppStartedtoState();
    } else if (event is LoggedIn) {
      yield* _mapLoggedIntoState();
    } else if (event is LoggedOut) {
      yield* _mapLoggedOuttoState();
    } else if (event is Registered) {
      yield* _mapRegisteredtoState();
    } else if (event is NewRegister) {
      yield* _mapNewRegistertoState();
    }
  }

  Stream<AuthenticationState> _mapAppStartedtoState() async* {
    try {
      final isSignedIn = await _auth.isSignedIn();
      if (isSignedIn) {
        final user = await _auth.currentUser();
        yield Authenticated(user);
      } else {
        yield Unauthenticated();
      }
    } catch (e) {
      yield Unauthenticated();
    }
  }

  Stream<AuthenticationState> _mapLoggedIntoState() async* {
    yield Authenticated(await _auth.currentUser());
  }

  Stream<AuthenticationState> _mapLoggedOuttoState() async* {
    await _auth.signOut();
    yield Unauthenticated();
  }

  Stream<AuthenticationState> _mapRegisteredtoState() async* {
    yield Unauthenticated();
  }

  Stream<AuthenticationState> _mapNewRegistertoState() async* {
    yield Unregistered();
  }
}
