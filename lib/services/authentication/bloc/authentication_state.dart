part of 'authentication_bloc.dart';

@immutable
abstract class AuthenticationState {}

class Uninitialized extends AuthenticationState {}

class Authenticated extends AuthenticationState {
  Authenticated(this.user);
  final User user;
}

class LoggedoutState extends AuthenticationState {}

class Unauthenticated extends AuthenticationState {}

class Unregistered extends AuthenticationState {}
