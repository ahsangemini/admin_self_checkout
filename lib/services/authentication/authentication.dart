import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:flutter/foundation.dart';

class User {
  User({@required this.uid, this.name, this.email, this.url});
  final String uid;
  final String name;
  final String email;
  final String url;
}

abstract class AuthBase {
  Stream<User> get onAuthStateChanged;
  bool isSignedIn();
  User currentUser();
  Future<User> signInWithEmailAndPassword(String email, String password);
  Future<User> createUserWithEmailAndPassword(String email, String password);
  Future<void> signOut();
  Future<void> updateUserInfo(String name, String url);
  Future<void> updateEmail(String email);
  Future<void> updatePassword(String password);
  //Future<void> createUser(String email,String password, String name, String url)
}

class Auth implements AuthBase {
  final _firebaseAuth = FirebaseAuth.instance;

  User _userFromFirebase(auth.User firebaseUser) {
    if (firebaseUser == null) {
      return null;
    }
    return User(
        uid: firebaseUser.uid,
        name: firebaseUser.displayName ?? "",
        email: firebaseUser.email,
        url: firebaseUser.photoURL ?? null);
  }

  @override
  Stream<User> get onAuthStateChanged {
    return _firebaseAuth.authStateChanges().map(_userFromFirebase);
  }

  @override
  bool isSignedIn() {
    final currentUser = _firebaseAuth.currentUser;
    return currentUser != null;
  }

  @override
  User currentUser() {
    final firebaseUser = _firebaseAuth.currentUser;
    return _userFromFirebase(firebaseUser);
  }

  @override
  Future<User> signInWithEmailAndPassword(String email, String password) async {
    final authResult = await _firebaseAuth.signInWithEmailAndPassword(
        email: email, password: password);
    return _userFromFirebase(authResult.user);
  }

  @override
  Future<User> createUserWithEmailAndPassword(
      String email, String password) async {
    final authResult = await _firebaseAuth.createUserWithEmailAndPassword(
        email: email, password: password);
    return _userFromFirebase(authResult.user);
  }

  @override
  Future<void> signOut() async {
    await _firebaseAuth.signOut();
  }

  @override
  Future<void> updateUserInfo(String name, String url) async {
    auth.User firebaseUser = _firebaseAuth.currentUser;
    await firebaseUser.updateProfile(displayName: name, photoURL: url);
  }

  @override
  Future<void> updateEmail(String email) async {
    auth.User firebaseUser = _firebaseAuth.currentUser;
    await firebaseUser.updateEmail(email);
  }

  @override
  Future<void> updatePassword(String password) async {
    auth.User firebaseUser = _firebaseAuth.currentUser;
    await firebaseUser.updatePassword(password);
    return null;
  }

  // @override
  // Future<void> createUser(String email, String password, String name, String url)  {
  //   return null;
  // }
}
