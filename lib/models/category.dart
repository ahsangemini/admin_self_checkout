import 'package:cloud_firestore/cloud_firestore.dart';

class CategoryModel {
  final String categoryId, description, name;
  final bool active, deleted, featured;
  final String images;

  CategoryModel({
    this.name,
    this.active,
    this.deleted,
    this.description,
    this.images,
    this.featured,
    this.categoryId,
  });

  factory CategoryModel.fromFirestore(DocumentSnapshot doc) {
    Map<String, dynamic> data = doc.data();
    return CategoryModel(
      categoryId: doc.id,
      name: data["name"] ?? null,
      active: data["active"] ?? false,
      deleted: data["deleted"] ?? false,
      description: data["description"] ?? null,
      featured: data["featured"] ?? false,
      images: List.from(data['images'])[0],
    );
  }
  Map<String, dynamic> toMap() {
    return {
      "name": name,
      "active": active,
      "description": description,
      "featured": featured,
      //"images": images,
    };
  }
}
