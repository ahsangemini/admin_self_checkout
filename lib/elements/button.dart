import 'package:admin_self_checkout/utilities/style.dart';
import 'package:flutter/material.dart';

class Button extends StatelessWidget {
  const Button({
    @required this.width,
    @required this.height,
    @required this.onPressed,
    @required this.child,
    this.color = AppColors.kYellow,
    Key key,
  }) : super(key: key);

  final double width;
  final double height;
  final Function onPressed;
  final Widget child;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.topCenter,
      children: <Widget>[
        Container(
          width: deviceWidth(context) * width - 50,
          height: 32,
          decoration: BoxDecoration(
            color: AppColors.kBlack,
            borderRadius: BorderRadius.circular(15),
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.27),
                blurRadius: 15.0,
                spreadRadius: 0.0,
                offset: Offset(
                  0.0,
                  16.0,
                ),
              )
            ],
          ),
        ),
        Container(
          width: deviceWidth(context) * width,
          height: height,
          decoration: BoxDecoration(
            color: color,
            borderRadius: BorderRadius.circular(15),
          ),
          child: FlatButton(
            padding: EdgeInsets.all(0),
            // elevation: 0.0,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
            onPressed: onPressed,
            child: child,
          ),
        ),
      ],
    );
  }
}

// return RaisedButton(
//   elevation: 8.0,
//   shape: RoundedRectangleBorder(
//       borderRadius: BorderRadius.all(Radius.circular(20.0))),
//   color: Colors.amber,
//   onPressed: onPressed,
//   child: Container(
//     width: deviceWidth(context) * width,
//     child: Text(
//       'Submit',
//       style: Theme.of(context).textTheme.caption,
//       textAlign: TextAlign.center,
//     ),
//   ),
// );
