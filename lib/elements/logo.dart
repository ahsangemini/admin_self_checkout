import 'package:admin_self_checkout/utilities/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Logo extends StatelessWidget {
  const Logo(this.width, {Key key}) : super(key: key);

  final double width;

  @override
  Widget build(BuildContext context) {
    return SvgPicture.asset(
      'assets/icons/amazon.svg',
      width: deviceWidth(context) * width,
      fit: BoxFit.contain,
    );
  }
}
