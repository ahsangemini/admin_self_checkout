import 'package:flutter/material.dart';

class SimpleCard extends StatelessWidget {
  const SimpleCard(
      {@required this.width,
      @required this.color,
      @required this.child,
      Key key})
      : super(key: key);

  final double width;
  final Color color;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      decoration:
          BoxDecoration(color: color, borderRadius: BorderRadius.circular(15)),
      child: child,
    );
  }
}
