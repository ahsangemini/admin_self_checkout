import 'package:admin_self_checkout/main.dart';
import 'package:admin_self_checkout/screens/login/login.dart';
import 'package:admin_self_checkout/services/authentication/authentication.dart';
import 'package:admin_self_checkout/services/authentication/bloc/authentication_bloc.dart';
import 'package:admin_self_checkout/utilities/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

enum PageType { Settings, Information, AddNewProduct }

abstract class PageContainerBase extends StatelessWidget {
  Widget get body;

  String get pageTitle;

  Widget get background;

  Widget get menuDrawer;

  Color get backgroundColor;

  Widget get floatingactionbutton;

  const PageContainerBase({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return
        //  BlocListener<AuthenticationBloc, AuthenticationState>(
        //   listener: (context, state) {
        //     if (state is Unauthenticated) {
        //       Navigator.of(context).pushAndRemoveUntil(
        //           MaterialPageRoute(
        //               builder: (context) => BlocProvider.value(
        //                   value: BlocProvider.of<AuthenticationBloc>(context),
        //                   child: MyApp(
        //                     auth: Provider.of<AuthBase>(context),
        //                   ))),
        //           (Route<dynamic> route) => false);
        //     }
        //   },
        //   child:
        Stack(
      children: <Widget>[
        Container(
          height: deviceHeight(context),
          width: deviceWidth(context),
          color: Theme.of(context).backgroundColor,
        ),
        background,
        Scaffold(
          backgroundColor: backgroundColor,
          appBar: AppBar(
            iconTheme: new IconThemeData(color: AppColors.kBlack),
            brightness: Brightness.light,
            backgroundColor: Colors.transparent,
            elevation: 0.0,
            title: Text(
              pageTitle,
              style: Theme.of(context).textTheme.title,
            ),
          ),
          drawer: menuDrawer,
          body: body,
          floatingActionButton: floatingactionbutton,
        ),
      ],
    );
    //  );
  }
}
