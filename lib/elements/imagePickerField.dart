import 'package:admin_self_checkout/utilities/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ImagePickerWidget extends StatelessWidget {
  ImagePickerWidget({Key key, this.onPressed1, this.onPressed2})
      : super(key: key);
  final Function onPressed1;
  final Function onPressed2;
  Widget _verticalDivider(double width, double height) {
    return Container(
      width: width,
      height: height,
      color: Colors.white,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      color: AppColors.kYellow,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8.0))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: IconButton(
              icon: SvgPicture.asset(
                "assets/icons/camera.svg",
                height: 20,
                color: AppColors.kWhite,
              ),
              iconSize: 20,
              onPressed: onPressed1,
            ),
          ),
          _verticalDivider(1, 20),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: IconButton(
              icon: Icon(FontAwesomeIcons.image,
                  color: AppColors.kWhite, size: 20),
              onPressed: onPressed2,
            ),
          ),
        ],
      ),
    );
  }
}
