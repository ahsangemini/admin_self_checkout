import 'dart:io';

import 'package:admin_self_checkout/elements/imagePickerField.dart';
import 'package:admin_self_checkout/utilities/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:form_bloc/form_bloc.dart';
import 'package:image_picker/image_picker.dart';

class ImageFieldBlocBuilder extends StatelessWidget {
  final InputFieldBloc<File> fileFieldBloc;
  final String url;
  final String downtext;
  const ImageFieldBlocBuilder({
    Key key,
    this.url,
    this.downtext = "Profile Image",
    @required this.fileFieldBloc,
  })  : assert(fileFieldBloc != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<InputFieldBloc<File>, InputFieldBlocState<File>>(
      bloc: fileFieldBloc,
      builder: (context, state) {
        return Column(
          children: <Widget>[
            Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(60),
              ),
              margin: EdgeInsets.zero,
              clipBehavior: Clip.antiAlias,
              elevation: 16,
              color: state.value != null ? Colors.grey[700] : Colors.white,
              child: Opacity(
                opacity: state.formBlocState.canSubmit ? 1 : 0.5,
                child: state.value != null
                    ? Image.file(
                        state.value,
                        height: 120,
                        width: 120,
                        fit: BoxFit.fill,
                      )
                    : (url == null)
                        ? SvgPicture.asset(
                            'assets/icons/user-circle.svg',
                            height: 100,
                            width: 100,
                            color: AppColors.kGrey,
                          )
                        : CircleAvatar(
                            radius: 55,
                            backgroundImage: NetworkImage(url),
                          ),
              ),
            ),
            // Opacity(

            //   opacity: state.formBlocState.canSubmit ? 1 : 0.5,
            //   child: CircleAvatar(
            //     radius: 55,
            //     backgroundImage: state.value != null
            //         ? AssetImage(state.value.path)
            //         : AssetImage(
            //             'assets/images/photo.png',
            //           ),
            //   ),
            // ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Text(downtext, style: Theme.of(context).textTheme.subhead),
            ),

            ImagePickerWidget(
              onPressed1: state.formBlocState.canSubmit
                  ? () async {
                      final image = await ImagePicker.pickImage(
                        source: ImageSource.camera,
                      );
                      if (image != null) {
                        fileFieldBloc.updateValue(image);
                      }
                    }
                  : null,
              onPressed2: state.formBlocState.canSubmit
                  ? () async {
                      final image = await ImagePicker.pickImage(
                        source: ImageSource.gallery,
                      );
                      if (image != null) {
                        fileFieldBloc.updateValue(image);
                      }
                    }
                  : null,
            ),
            // AnimatedContainer(
            //   duration: Duration(milliseconds: 300),
            //   height: state.canShowError ? 30 : 0,
            //   child: SingleChildScrollView(
            //     physics: ClampingScrollPhysics(),
            //     child: Column(
            //       children: <Widget>[
            //         SizedBox(height: 8),
            //         Text(
            //           'Please select an Image',
            //           style: TextStyle(
            //             color: Theme.of(context).errorColor,
            //           ),
            //         ),
            //       ],
            //     ),
            //   ),
            // ),
          ],
        );
      },
    );
  }
}
