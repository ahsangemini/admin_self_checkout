import 'package:admin_self_checkout/services/authentication/bloc/authentication_bloc.dart';
import 'package:admin_self_checkout/widgets/custom_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:admin_self_checkout/admin/screens/brands.dart';
import 'package:admin_self_checkout/admin/screens/category_admin.dart';
import 'package:admin_self_checkout/admin/screens/orders.dart';
import 'package:admin_self_checkout/admin/screens/products.dart';
import 'package:admin_self_checkout/admin/screens/slider_admin.dart';
import 'package:admin_self_checkout/admin/screens/users.dart';
import 'package:admin_self_checkout/services/authentication/authentication.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

List<Map> navItems = [
  {"icon": "stores", "label": "STORES"},
  {"icon": "orders", "label": "ORDERS"},
  {"icon": "referrals", "label": "REFERRALS"},
  {"icon": "support", "label": "SUPPORT"},
  {"icon": "person", "label": "ACCOUNT"},
];

class CustomDrawer extends StatelessWidget {
  const CustomDrawer({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: new Material(
        color: Colors.amber,
        child: new Container(
          padding: EdgeInsets.symmetric(vertical: 48.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                height: 30,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 16.0, right: 16),
                child: Text(
                  "Ahsan 62715",
                  textAlign: TextAlign.center,
                  style: new TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 0.73,
                    fontSize: 24,
                  ),
                ),
              ),
              SizedBox(
                height: 16,
              ),
              Divider(
                color: Colors.white,
              ),
              SizedBox(
                height: 16,
              ),
              ListTile(
                onTap: () {
                  // Navigator.pop(context);
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (_) => UsersAdmin()));
                },
                leading: Icon(
                  FontAwesomeIcons.users,
                  size: 24.0,
                  color: Colors.white,
                ),
                title: Text("users",
                    style: new TextStyle(
                        color: Colors.white,
                        fontFamily: 'SF',
                        fontWeight: FontWeight.normal,
                        fontSize: 24.0)),
              ),
              ListTile(
                onTap: () {
                  // Navigator.pop(context);
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (_) => ProductsAdmin()));
                },
                leading: Icon(
                  FontAwesomeIcons.gifts,
                  size: 24.0,
                  color: Colors.white,
                ),
                title: Text("Products",
                    style: new TextStyle(
                        color: Colors.white,
                        fontFamily: 'SF',
                        fontWeight: FontWeight.normal,
                        fontSize: 24.0)),
              ),
              ListTile(
                onTap: () {
                  Navigator.of(context).push(FadeRoute(page: AdminOrders()));
                },
                leading: Icon(
                  FontAwesomeIcons.solidHandPaper,
                  size: 24.0,
                  color: Colors.white,
                ),
                title: Text("orders",
                    style: new TextStyle(
                        color: Colors.white,
                        fontFamily: 'SF',
                        fontWeight: FontWeight.normal,
                        fontSize: 24.0)),
              ),
              ListTile(
                onTap: () {
                  Navigator.of(context).push(FadeRoute(page: CategoryAdmin()));
                },
                leading: Icon(
                  FontAwesomeIcons.list,
                  size: 24.0,
                  color: Colors.white,
                ),
                title: Text("category",
                    style: new TextStyle(
                        color: Colors.white,
                        fontFamily: 'SF',
                        fontWeight: FontWeight.normal,
                        fontSize: 24.0)),
              ),
              ListTile(
                onTap: () {
                  Navigator.of(context).push(FadeRoute(page: BrandsAdmin()));
                },
                leading: Icon(
                  FontAwesomeIcons.brandsFontAwesomeLogoFull,
                  size: 24.0,
                  color: Colors.white,
                ),
                title: Text("brands",
                    style: new TextStyle(
                        color: Colors.white,
                        fontFamily: 'SF',
                        fontWeight: FontWeight.normal,
                        fontSize: 24.0)),
              ),
              ListTile(
                onTap: () {
                  Navigator.of(context).push(FadeRoute(page: SlidersAdmin()));
                },
                leading: Icon(
                  FontAwesomeIcons.tag,
                  size: 24.0,
                  color: Colors.white,
                ),
                title: Text("slider",
                    style: new TextStyle(
                        color: Colors.white,
                        fontFamily: 'SF',
                        fontWeight: FontWeight.normal,
                        fontSize: 24.0)),
              ),
              Spacer(),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Divider(
                    color: Colors.white,
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  InkWell(
                    onTap: () {
                      BlocProvider.of<AuthenticationBloc>(context).add(
                        LoggedOut(),
                      );
                      Navigator.of(context).pop();
                    },
                    child: ListTile(
                      leading: Icon(
                        Icons.exit_to_app,
                        size: 24.0,
                        color: Colors.white,
                      ),
                      title: Text("signout",
                          style: new TextStyle(
                              color: Colors.white,
                              fontFamily: 'SF',
                              fontWeight: FontWeight.normal,
                              fontSize: 24.0)),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
