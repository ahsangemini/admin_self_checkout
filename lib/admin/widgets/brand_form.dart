import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:admin_self_checkout/admin/bloc/form_bloc/bloc.dart';
import 'package:admin_self_checkout/models/brand.dart';
import 'package:admin_self_checkout/widgets/progress.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';

class BrandsForm extends StatefulWidget {
  final BrandModel brand;

  BrandsForm({Key key, @required this.brand}) : super(key: key);

  _BrandsFormState createState() => _BrandsFormState();
}

class _BrandsFormState extends State<BrandsForm> {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final globalKey = GlobalKey<ScaffoldState>();
  bool isFeatured = false;
  bool _saving = false;
  PickedFile assetImages;
  List<String> oldImages;
  BrandModel brandData;
  final ImagePicker _imagePicker = ImagePicker();

  FormBlocBloc _formBlocBloc;

  @override
  void initState() {
    super.initState();
    _formBlocBloc = BlocProvider.of<FormBlocBloc>(context);
    final BrandModel brand = widget.brand;
    brandData = widget.brand;
    isFeatured = brand != null ? brand.featured : false;
    _nameController.text = brand != null ? brand.name : null;
    _descriptionController.text = brand != null ? brand.description : null;
  }

  void updateOldImages(urlNewImages) {
    print("upatedImages $urlNewImages");
    setState(() {
      oldImages = urlNewImages;
    });
  }

  void updateAssetImages(assetNewImages) {
    print("upatedImages $assetImages");
    setState(() {
      assetImages = assetNewImages;
    });
  }

  void showNotification(BuildContext context, String message, Color color) {
    final snackBar = SnackBar(
      backgroundColor: color,
      content: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(message),
        ],
      ),
    );
    globalKey.currentState.showSnackBar(snackBar);
  }

  ///
  ///
  ///  OnSubmit method -- You have to upload the images.
  ///
  ///

  void onSubit(BuildContext context) async {
    print(_nameController.text.length);
    print(_descriptionController.text.length);
    if (_nameController.text.length <= 0 ||
        _descriptionController.text.length <= 0) {
      showNotification(context, "Add all the details", Colors.redAccent);
    } else {
      _formBlocBloc.add(
        AddAdminBrands(
          brand: BrandModel(
              active: true,
              name: _nameController.text,
              description: _descriptionController.text,
              images: assetImages.path,

              /// ----> Change this to send all the images handle differently for edit and add method. TODO:
              featured: isFeatured),
        ),
      );
    }
  }

  void onUpdate(BuildContext context) async {
    print(_nameController.text.length);
    print(_descriptionController.text.length);
    if (_nameController.text.length <= 0 ||
        _descriptionController.text.length <= 0) {
      showNotification(context, "Add all the details", Colors.redAccent);
    } else {
      // Onupdate is Called

      print("onUpdate is called");
      print(assetImages);
      print("OLD IMAGES ON UPDATE $oldImages");

      _formBlocBloc.add(
        EditAdminBrand(
            brand: BrandModel(
                brandId: brandData.brandId,
                active: true,
                name: _nameController.text,
                description: _descriptionController.text,
                images: assetImages.path,

                /// ----> Change this to send all the images handle differently for edit and add method. TODO:
                featured: isFeatured),
            oldImages: oldImages),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
        bloc: _formBlocBloc,
        listener: (BuildContext context, FormBlocState state) {
          print("STATE ${state.isSubmitting}");
          if (state.isFailure) {
            showNotification(context, "Error adding brand", Colors.redAccent);
          } else if (state.isSubmitting) {
            setState(() {
              _saving = true;
            });
          } else if (state.isSuccess) {
            setState(() {
              _saving = false;
            });
            showNotification(context, "Brand added successfully", Colors.green);
            Navigator.pop(context);
          }
        },
        child: ModalProgressHUD(
          child: Scaffold(
            key: globalKey,
            appBar: AppBar(
              backgroundColor: Colors.amber,
              leading: IconButton(
                color: Colors.white,
                onPressed: () => Navigator.of(context).pop(),
                icon: Icon(Icons.arrow_back_ios, color: Colors.white),
              ),
              title: Text(
                brandData != null ? "Edit Brand" : "Add Brand",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 22,
                    fontWeight: FontWeight.w700),
              ),
            ),
            body: BlocBuilder(
              bloc: _formBlocBloc,
              builder: (BuildContext context, FormBlocState state) {
                print("NEW SATET ${state.isSubmitting}");
                return SingleChildScrollView(
                  child: Container(
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: <Widget>[
                          Card(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Divider(),
                                Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 20, vertical: 8),
                                  child: TextFormField(
                                    controller: _nameController,
                                    maxLength: 64,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      labelText: "Brand Name",
                                    ),
                                    autovalidate: true,
                                    autocorrect: false,
                                    validator: (value) {
                                      return value.length < 3
                                          ? 'Invalid Brand name. Atleast 3 letter is needed.'
                                          : null;
                                    },
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 20, vertical: 8),
                                  child: TextFormField(
                                    controller: _descriptionController,
                                    maxLength: 64,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      labelText: "Brand description",
                                    ),
                                    autovalidate: true,
                                    autocorrect: false,
                                    validator: (value) {
                                      return value.length < 3
                                          ? 'Invalid input. Atleast 3 letter is needed.'
                                          : null;
                                    },
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        "Featured",
                                        style: TextStyle(
                                            fontSize: 24.0,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Switch(
                                        value: isFeatured,
                                        onChanged: (value) {
                                          setState(() {
                                            isFeatured = value;
                                          });
                                        },
                                        activeTrackColor: Colors.amber,
                                        activeColor: Colors.amber,
                                      ),
                                    ],
                                  ),
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    GestureDetector(
                                      child: Card(
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                        ),
                                        margin: EdgeInsets.zero,
                                        clipBehavior: Clip.antiAlias,
                                        elevation: 16,
                                        color: assetImages != null
                                            ? Colors.grey[700]
                                            : Colors.white,
                                        child: Opacity(
                                            opacity:
                                                (assetImages != null) ? 1 : 0.5,
                                            child: assetImages != null
                                                ? Image.file(
                                                    File(assetImages.path),
                                                    height: 60,
                                                    width: 60,
                                                    fit: BoxFit.cover,
                                                  )
                                                : Container(
                                                    height: 60,
                                                    width: 60,
                                                    child: Icon(
                                                        Icons
                                                            .add_photo_alternate,
                                                        color: Colors.black87,
                                                        size: 35),
                                                  )),
                                      ),
                                      onTap: () async {
                                        // await Permission.camera.request();
                                        if (await Permission.photos.isGranted) {
                                          assetImages =
                                              await _imagePicker.getImage(
                                                  source: ImageSource.gallery);
                                          setState(() {});
                                        }
                                      },
                                    ),
                                  ],
                                ),
                                // Container(
                                //   height: 300,
                                //   child: picker.ImagePicker(
                                //     oldImages: oldImages,
                                //     pickerTitle: "Pick Images",
                                //     imageCount: 6,
                                //     updateAssetImages: updateAssetImages,
                                //     updateOldImages: updateOldImages,
                                //   ),
                                // ),
                                Padding(
                                  padding: const EdgeInsets.all(16.0),
                                  child: Center(
                                    child: Container(
                                      width: MediaQuery.of(context).size.width *
                                          0.4,
                                      height: 40,
                                      child: RaisedButton(
                                        color: Colors.amber,
                                        child: Text(
                                          "SUBMIT",
                                          style: TextStyle(
                                              fontSize: 22,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white),
                                        ),
                                        onPressed: () => widget.brand != null
                                            ? onUpdate(context)
                                            : onSubit(context),
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
          inAsyncCall: _saving,
        ));
  }
}
