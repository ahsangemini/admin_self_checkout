class AppRoutes {
  static final splashScreen = '/';
  static final settings = '/settings';
  static final addProduct = '/addProduct';
}
