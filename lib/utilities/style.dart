import 'package:flutter/material.dart';

class AppColors {
  static const kBlack = Color(0xFF3D3D4E);
  static const kGrey = Color(0xFFAEAEBC);
  static const kLightGrey = Color(0xFFECECF4);
  static const kYellow = Color(0xFFFF9900);
  static const kLightYellow = Color(0xFFFFC166);
  static const kLighterYellow = Color(0xFFFFF4E5);
  static const kWhite = Colors.white;
}

class Spacing {
  static const double _matGridUnit = 8.0;
  static double matGridUnit({scale = 1}) {
    // Material design grid uses multiples of 8
    // Multiples of 4 are acceptable if needed
    // Only accept numbers that are full or half units of _matGridUnit
    assert(scale % .5 == 0);
    return _matGridUnit * scale;
  }
}

final ThemeData kTheme = _buildmyTheme();
ThemeData _buildmyTheme() {
  final ThemeData base = ThemeData.light();
  return base.copyWith(
    colorScheme: ColorScheme(
      primary: Colors.amber,
      primaryVariant: Colors.amber,
      secondary: AppColors.kYellow,
      secondaryVariant: AppColors.kYellow,
      surface: AppColors.kYellow,
      background: AppColors.kWhite,
      error: AppColors.kYellow,
      onPrimary: AppColors.kWhite,
      onSecondary: AppColors.kYellow,
      onSurface: AppColors.kYellow,
      onBackground: AppColors.kWhite,
      onError: AppColors.kYellow,
      brightness: Brightness.light,
    ),
    primaryColor: AppColors.kWhite,
    backgroundColor: AppColors.kWhite,
    accentColor: AppColors.kLighterYellow,
    scaffoldBackgroundColor: AppColors.kWhite,
    errorColor: AppColors.kYellow,
    buttonTheme: base.buttonTheme.copyWith(
      buttonColor: AppColors.kYellow,
      textTheme: ButtonTextTheme.normal,
    ),
    textTheme: TextTheme(
      headline: TextStyle(
        fontSize: 25,
        color: AppColors.kBlack,
        fontWeight: FontWeight.w700,
        fontFamily: 'Helvetica',
      ),
      title: TextStyle(
        fontSize: 20,
        color: AppColors.kBlack,
        fontWeight: FontWeight.w700,
        fontFamily: 'Helvetica',
      ),
      subhead: TextStyle(
        fontSize: 16,
        color: AppColors.kBlack,
        fontWeight: FontWeight.w500,
        fontFamily: 'Helvetica',
      ),
      body1: TextStyle(
        fontSize: 14,
        color: AppColors.kBlack,
        fontWeight: FontWeight.w500,
        fontFamily: 'Helvetica',
      ),
      subtitle: TextStyle(
          fontSize: 12,
          color: AppColors.kBlack,
          fontWeight: FontWeight.w500,
          fontFamily: 'Helvetica',
          height: 1.25),
    ),
  );
}

double deviceWidth(BuildContext context) {
  return MediaQuery.of(context).size.width;
}

double deviceHeight(BuildContext context) {
  return MediaQuery.of(context).size.height;
}
